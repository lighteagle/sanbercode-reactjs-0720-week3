import React, { Component } from "react";

class Timer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      time: 100,
    };
  }

  componentDidMount() {
    if (this.props.start !== undefined) {
      this.setState({ time: this.props.start });
    }
    this.timerID = setInterval(() => this.tick(), 1000);
  }

  componentWillUnmount() {
    clearInterval(this.timerID);
  }

  componentDidUpdate() {
    if (this.state.time === 0) {
      clearInterval(this.timerID);
    }
  }

  tick() {
    this.setState({
      time: this.state.time - 1,
    });
    console.log(this.state.time);
  }

  render() {
    let jam = new Date().toLocaleTimeString("en-US", {
      hour: "numeric",
      hour12: true,
      minute: "numeric",
      second: "numeric",
    });
    return (
      <div>
        {this.state.time > 0 && (
          <h1 style={{ textAlign: "center" }}>
            Sekarang Jam : {jam} Hitung Mundur : {this.state.time}
          </h1>
        )}
      </div>
    );
  }
}
export default Timer;
