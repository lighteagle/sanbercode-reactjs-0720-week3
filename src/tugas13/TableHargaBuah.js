import React, { Component } from "react";

class TableHargaBuah extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataHargaBuah: [
        { nama: "Semangka", harga: 10000, berat: 1000 },
        { nama: "Anggur", harga: 40000, berat: 500 },
        { nama: "Strawberry", harga: 30000, berat: 400 },
        { nama: "Jeruk", harga: 30000, berat: 1000 },
        { nama: "Mangga", harga: 30000, berat: 500 },
      ],
      inputNama: "",
      inputHarga: "",
      inputBerat: "",
      indexOfForm: -1,
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleEdit = this.handleEdit.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
  }
  handleDelete(event) {
    let dataHargaBuah = this.state.dataHargaBuah;
    dataHargaBuah.splice(event.target.value, 1);
    this.setState({ dataHargaBuah });
  }
  handleEdit(event) {
    const index = event.target.value;
    let { nama, harga, berat } = this.state.dataHargaBuah[index];
    this.setState({
      inputNama: nama,
      inputHarga: harga,
      inputBerat: berat / 1000,
      indexOfForm: index,
    });
  }

  handleChange(event) {
    const { name, value } = event.target;
    this.setState({
      [name]: value,
    });
  }
  handleSubmit(event) {
    event.preventDefault();

    let dataHargaBuah = this.state.dataHargaBuah;
    let nama = this.state.inputNama;
    let harga = this.state.inputHarga;
    let berat = this.state.inputBerat * 1000;
    let index = this.state.indexOfForm;

    if (index === -1) {
      dataHargaBuah = [...dataHargaBuah, { nama, harga, berat }];
    } else {
      dataHargaBuah[index] = { nama, harga, berat };
    }
    this.setState({
      dataHargaBuah: dataHargaBuah,
      inputNama: "",
      inputHarga: "",
      inputBerat: "",
      indexOfForm: -1,
    });
  }

  render() {
    return (
      <div id="form-harga-buah">
        <h1>Tabel Harga Buah</h1>
        <table id="tabel-harga">
          <thead>
            <tr>
              <th>Nama</th>
              <th>Harga</th>
              <th>Berat</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
            {this.state.dataHargaBuah.map((val, index) => {
              return (
                <tr key={index}>
                  <td>{val.nama}</td>
                  <td>{val.harga}</td>
                  <td>{val.berat / 1000} kg</td>
                  <td>
                    <button onClick={this.handleEdit} value={index}>
                      Edit
                    </button>
                    &nbsp;
                    <button onClick={this.handleDelete} value={index}>
                      Delete
                    </button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>

        <h1>Form Peserta</h1>
        <form onSubmit={this.handleSubmit}>
          <table id="table-form">
            <tbody>
              <tr>
                <td>Nama</td>
                <td>
                  <input
                    type="text"
                    id="inputNama"
                    name="inputNama"
                    value={this.state.inputNama}
                    onChange={this.handleChange}
                  />
                </td>
              </tr>
              <tr>
                <td>Harga</td>
                <td>
                  <input
                    type="text"
                    id="inputHarga"
                    name="inputHarga"
                    value={this.state.inputHarga}
                    onChange={this.handleChange}
                  />
                </td>
              </tr>
              <tr>
                <td>Berat</td>
                <td>
                  <input
                    type="text"
                    id="inputBerat"
                    name="inputBerat"
                    value={this.state.inputBerat}
                    onChange={this.handleChange}
                  />{" "}
                  Kg
                </td>
              </tr>
              <tr>
                <td colSpan="2" style={{ textAlign: "center", height: 50 }}>
                  <button style={{ padding: "5px 20px" }}>submit</button>
                </td>
              </tr>
            </tbody>
          </table>
        </form>
      </div>
    );
  }
}

export default TableHargaBuah;
