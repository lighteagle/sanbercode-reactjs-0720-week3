import React from "react";
import { Switch, Link, Route } from "react-router-dom";
import TableHargaBuah from "../tugas11/TableHargaBuah";
import Timer from "../tugas12/Timer";
import UsingClass from "../tugas13/TableHargaBuah";
import ApiIntegration from "../tugas14/TableHargaBuah";
import DataBuah from "./DataBuah";

const Routes = () => {
  return (
    <>
      <nav>
        <Link to="/">Home</Link>
        <Link to="/timer">Timer</Link>
        <Link to="/using-class">Using Class</Link>
        <Link to="/api-integration">API Integration</Link>
        <Link to="/using-context">Using context</Link>
      </nav>
      <Switch>
        <Route exact path="/" component={TableHargaBuah}></Route>
        <Route path="/timer" component={Timer} />
        <Route path="/using-class" component={UsingClass} />
        <Route path="/api-integration" component={ApiIntegration} />
        <Route path="/using-context" component={DataBuah} />
      </Switch>
    </>
  );
};

export default Routes;
