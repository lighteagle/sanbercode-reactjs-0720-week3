import React, { useContext, useState } from "react";
import { DataBuahContext } from "./DataBuahContext";
import axios from "axios";
const urlAPI = "http://backendexample.sanbercloud.com/api/fruits";

const DataBuahForm = () => {
  const [dataBuah, setDataBuah, input, setInput] = useContext(DataBuahContext);

  const handleChange = (event) => {
    const { name, value } = event.target;
    switch (name) {
      case "inputNama":
        setInput({ ...input, name: value });
        break;
      case "inputHarga":
        setInput({ ...input, price: value });
        break;
      case "inputBerat":
        setInput({ ...input, weight: value });
        break;
      default:
        break;
    }
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    const { name, price, weight, selectedId } = input;

    if (selectedId === -1) {
      axios
        .post(urlAPI, { name, price, weight })
        .then((res) => {
          console.log(res.data);
          setInput({ ...input, changeData: input.changeData + 1 });
        })
        .then(() =>
          setInput({ ...input, name: "", price: "", weight: 0, selectedId: -1 })
        );
    } else {
      axios
        .put(`${urlAPI}/${parseInt(selectedId)}`, { name, price, weight })
        .then((res) => {
          console.log(res);
          setInput({ ...input, changeData: input.changeData + 1 });
        })
        .then(() =>
          setInput({ ...input, name: "", price: "", weight: 0, selectedId: -1 })
        );
    }
  };
  return (
    <div id="form-harga-buah">
      <h1>Form Data Buah</h1>
      <form onSubmit={handleSubmit}>
        <table id="table-form">
          <tbody>
            <tr>
              <td>Nama</td>
              <td>
                <input
                  type="text"
                  id="inputNama"
                  name="inputNama"
                  value={input.name}
                  onChange={handleChange}
                />
              </td>
            </tr>
            <tr>
              <td>Harga</td>
              <td>
                <input
                  type="text"
                  id="inputHarga"
                  name="inputHarga"
                  value={input.price}
                  onChange={handleChange}
                />
              </td>
            </tr>
            <tr>
              <td>Berat</td>
              <td>
                <input
                  type="text"
                  id="inputBerat"
                  name="inputBerat"
                  value={input.weight}
                  onChange={handleChange}
                />{" "}
                Kg
              </td>
            </tr>
            <tr>
              <td colSpan="2" style={{ textAlign: "center", height: 50 }}>
                <button style={{ padding: "5px 20px" }}>submit</button>
              </td>
            </tr>
          </tbody>
        </table>
      </form>
    </div>
  );
};

export default DataBuahForm;
