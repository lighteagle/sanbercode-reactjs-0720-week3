import React, { useState, createContext } from "react";

export const DataBuahContext = createContext();

export const DataBuahProvider = (props) => {
  const [dataBuah, setDataBuah] = useState([]);
  const [input, setInput] = useState({
    name: "",
    price: "",
    weight: 0,
    selectedId: -1,
    changeData: 0,
  });

  return (
    <DataBuahContext.Provider value={[dataBuah, setDataBuah, input, setInput]}>
      {props.children}
    </DataBuahContext.Provider>
  );
};
