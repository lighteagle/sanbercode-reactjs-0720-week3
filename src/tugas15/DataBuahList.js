import React, { useContext, useEffect } from "react";
import { DataBuahContext } from "./DataBuahContext";
import axios from "axios";

const DataBuahList = () => {
  const [dataBuah, setDataBuah, input, setInput] = useContext(DataBuahContext);
  const urlAPI = "http://backendexample.sanbercloud.com/api/fruits";

  useEffect(() => {
    axios.get(urlAPI).then((res) => {
      console.log(res.data);
      setDataBuah(res.data);
    });
  }, [setDataBuah, input.changeData]);

  const handleEdit = (event) => {
    const selectedId = event.target.value;
    const { name, price, weight } = dataBuah.find(
      (x) => x.id === parseInt(selectedId)
    );
    setInput({ name, price, weight: weight / 1000, selectedId });
  };

  const handleDelete = (event) => {
    const idDataHargaBuah = parseInt(event.target.value);
    axios
      .delete(`${urlAPI}/${idDataHargaBuah}`)
      .then((res) => {
        console.log(res);
      })
      .then(() => setInput({ ...input, changeData: input.changeData + 1 }));
  };
  return (
    <div id="form-harga-buah">
      <h1>Tabel Harga Buah</h1>
      <table id="tabel-harga">
        <thead>
          <tr>
            <th>Nama</th>
            <th>Harga</th>
            <th>Berat</th>
            <th>Aksi</th>
          </tr>
        </thead>
        <tbody>
          {dataBuah.map((val, index) => {
            return (
              <tr key={index}>
                <td>{val.name}</td>
                <td>{val.price}</td>
                <td>{val.weight / 1000} kg</td>
                <td>
                  <button onClick={handleEdit} value={val.id}>
                    Edit
                  </button>
                  &nbsp;
                  <button onClick={handleDelete} value={val.id}>
                    Delete
                  </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
};

export default DataBuahList;
