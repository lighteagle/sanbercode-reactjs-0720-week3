import React, { useState, useEffect } from "react";
import axios from "axios";

const TableHargaBuah = () => {
  const [dataHargaBuah, setDataHargaBuah] = useState([]);
  const [changeData, setChangeData] = useState(0);
  const urlAPI = "http://backendexample.sanbercloud.com/api/fruits";
  const [inputNama, setInputNama] = useState("");
  const [inputHarga, setInputHarga] = useState("");
  const [inputBerat, setInputBerat] = useState("");
  const [indexOfForm, setIndexOfForm] = useState(-1);

  useEffect(() => {
    axios.get(urlAPI).then((res) => {
      console.log(res.data);
      setDataHargaBuah(res.data);
    });
  }, [changeData]);

  const handleDelete = (event) => {
    const idDataHargaBuah = parseInt(event.target.value);

    axios.delete(`${urlAPI}/${idDataHargaBuah}`).then((res) => {
      console.log(res);
      setChangeData(changeData + 1);
    });
  };
  const handleEdit = (event) => {
    const idDataHargaBuah = event.target.value;
    const { name, price, weight } = dataHargaBuah.find(
      (x) => x.id === parseInt(idDataHargaBuah)
    );
    setInputNama(name);
    setInputHarga(price);
    setInputBerat(weight / 1000);
    setIndexOfForm(idDataHargaBuah);
  };

  const handleChange = (event) => {
    const { name, value } = event.target;
    switch (name) {
      case "inputNama":
        setInputNama(value);
        break;
      case "inputHarga":
        setInputHarga(value);
        break;
      case "inputBerat":
        setInputBerat(value);
        break;
      default:
        break;
    }
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    const name = inputNama;
    const price = inputHarga;
    const weight = inputBerat * 1000;
    const index = indexOfForm;
    if (index === -1) {
      axios.post(urlAPI, { name, price, weight }).then((res) => {
        console.log(res.data);
        setChangeData(changeData + 1);
      });
    } else {
      axios.put(`${urlAPI}/${index}`, { name, price, weight }).then((res) => {
        console.log(res);
        setChangeData(changeData + 1);
      });
    }

    setInputNama("");
    setInputHarga("");
    setInputBerat("");
    setIndexOfForm(-1);
  };

  return (
    <div id="form-harga-buah">
      <h1>Tabel Harga Buah</h1>
      <table id="tabel-harga">
        <thead>
          <tr>
            <th>Nama</th>
            <th>Harga</th>
            <th>Berat</th>
            <th>Aksi</th>
          </tr>
        </thead>
        <tbody>
          {dataHargaBuah.map((val, index) => {
            return (
              <tr key={index}>
                <td>{val.name}</td>
                <td>{val.price}</td>
                <td>{val.weight / 1000} kg</td>
                <td>
                  <button onClick={handleEdit} value={val.id}>
                    Edit
                  </button>
                  &nbsp;
                  <button onClick={handleDelete} value={val.id}>
                    Delete
                  </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>

      <h1>Form Peserta</h1>
      <form onSubmit={handleSubmit}>
        <table id="table-form">
          <tbody>
            <tr>
              <td>Nama</td>
              <td>
                <input
                  type="text"
                  id="inputNama"
                  name="inputNama"
                  value={inputNama}
                  onChange={handleChange}
                />
              </td>
            </tr>
            <tr>
              <td>Harga</td>
              <td>
                <input
                  type="text"
                  id="inputHarga"
                  name="inputHarga"
                  value={inputHarga}
                  onChange={handleChange}
                />
              </td>
            </tr>
            <tr>
              <td>Berat</td>
              <td>
                <input
                  type="text"
                  id="inputBerat"
                  name="inputBerat"
                  value={inputBerat}
                  onChange={handleChange}
                />{" "}
                Kg
              </td>
            </tr>
            <tr>
              <td colSpan="2" style={{ textAlign: "center", height: 50 }}>
                <button style={{ padding: "5px 20px" }}>submit</button>
              </td>
            </tr>
          </tbody>
        </table>
      </form>
    </div>
  );
};

export default TableHargaBuah;
