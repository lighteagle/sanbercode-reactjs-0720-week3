import React from "react";

let dataHargaBuah = [
  { nama: "Semangka", harga: 10000, berat: 1000 },
  { nama: "Anggur", harga: 40000, berat: 500 },
  { nama: "Strawberry", harga: 30000, berat: 400 },
  { nama: "Jeruk", harga: 30000, berat: 1000 },
  { nama: "Mangga", harga: 30000, berat: 500 },
];

const TableHargaBuah = () => {
  return (
    <div id="form-harga-buah">
      <h1>Tabel Harga Buah</h1>
      <table id="tabel-harga">
        <thead>
          <th>Nama</th>
          <th>Harga</th>
          <th>Berat</th>
        </thead>
        <TableBody dataTable={dataHargaBuah} />
      </table>
    </div>
  );
};

const TableBody = ({ dataTable }) => {
  return (
    <tbody>
      {dataTable.map((data) => {
        return (
          <tr>
            <td>{data.nama}</td>
            <td>{data.harga}</td>
            <td>{data.berat / 1000} kg</td>
          </tr>
        );
      })}
    </tbody>
  );
};

export default TableHargaBuah;
